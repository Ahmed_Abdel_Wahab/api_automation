package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.When;
import org.apache.http.client.methods.HttpGet;



public class getUserInfoCode{

	String User;
	StringBuffer UserResult = new StringBuffer();
	

	
	  @When("^I call Get user info code api$")
	  public void Get_User_Info_Code() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  String Id =sharedSteps.CreatedUser12Digit.toString();
		  //Build Get URL plus User ID 'Easier way'
		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/Info/"+Id);
		  HttpGet request = new HttpGet(builder.build());
			// add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  System.out.println("Built URL : " + builder);
		  System.out.println("Request : " + request);

		  sharedSteps.response = client.execute(request);		  
		  System.out.println("Get user info Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
		  }		  
		  sharedSteps.responseContent= result.toString();
	      System.out.println("Get User Info by UserID Response: "+sharedSteps.responseContent);
	      //Save to file
		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/GetUserInfoBy12Digit"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	  }
	
	
