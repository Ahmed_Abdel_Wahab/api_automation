package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import gherkin.deps.com.google.gson.JsonParser;

import org.apache.http.client.methods.HttpPost;



public class requestResetPasswordByID{

	String User;
	StringBuffer UserResult = new StringBuffer();

	
	  @When("^I call request reset password ByID api$")
	  public void request_Reset_Password_ByID() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  //Confirm user API
		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/RequestResetPassword/UserID");	
		  String Id =sharedSteps.CreatedUser12Digit.toString().replaceAll("^\"|\"$", "");
		  builder.addParameter("userID", Id);
		  HttpPost request = new HttpPost (builder.build());
			// add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  System.out.println("Request : " + request);

		  sharedSteps.response = client.execute(request);
		  System.out.println("Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
	      }
		  sharedSteps.responseContent= result.toString();
		  System.out.println("Response : " + sharedSteps.responseContent);
		  
		  //Extract Reset Token for further APIs
	      JsonParser parser = new JsonParser();
	      Object resultObject = parser.parse(sharedSteps.responseContent);
	      JsonObject obj =(JsonObject)resultObject;
	      sharedSteps.VerificationToken=obj.get("result").getAsString();
	      System.out.println("Reset Token : " +  sharedSteps.VerificationToken);

		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/requestResetPasswordByID"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	
	
}