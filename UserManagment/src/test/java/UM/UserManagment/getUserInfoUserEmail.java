package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.When;
import org.apache.http.client.methods.HttpGet;



public class getUserInfoUserEmail{

	String User;
	StringBuffer UserResult = new StringBuffer();
	

	
	  @When("^I call Get user info user email api$")
	  public void Get_user_info_user_email() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  
		  //Build Get URL plus unique created UserName 'Easier way'
		  String UserName =sharedSteps.UserName.toString().replaceAll("^\"|\"$", "");

		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/Info/"+UserName);
		  HttpGet request = new HttpGet(builder.build());
		  
		  // add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  System.out.println("Built URL : " + builder);
		  System.out.println("Request : " + request);

		  sharedSteps.response = client.execute(request);		  
		  System.out.println("Get user info Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
		  }		  
		  sharedSteps.responseContent= result.toString();
	      System.out.println("Get User Info by UserEmail Response: "+sharedSteps.responseContent);
	      
	      //Save to file
		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/GetUserInfoByUserEmail"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	  }
	
	
