package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import gherkin.deps.com.google.gson.JsonParser;

import org.apache.http.client.methods.HttpPost;



public class loginUser{

	String User;
	StringBuffer UserResult = new StringBuffer();

	@Given("^I have login user data$")
	public void get_user_data() throws Throwable {
		File UserData = new File("./data/LoginUser.txt"); 
		String line=null;
		try {
				BufferedReader br = new BufferedReader(new FileReader(UserData));
				while ((line= br.readLine()) != null)
				{	
					UserResult.append(line);
				}
				User = UserResult.toString();
				br.close();
			}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("User Login Data :"+ UserResult);
	}

	
	  @When("^I call login user api$")
	  public void login_user() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  //Confirm user API
		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/Login");	

		  HttpPost request = new HttpPost (builder.build());
			// add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  JsonParser parser = new JsonParser();
	      Object resultObject = parser.parse(User);
	      JsonObject userJson =(JsonObject)resultObject;

	      userJson.addProperty("loginName", sharedSteps.UserName.toString().replaceAll("^\"|\"$", ""));
	      userJson.addProperty("password", sharedSteps.Password);
	      userJson.addProperty("portalID", sharedSteps.PortalID.replaceAll("^\"|\"$", ""));

          request.setEntity(new StringEntity(userJson.toString(),ContentType.create("application/json"))); 
		  System.out.println("Request: " + userJson);

		  sharedSteps.response = client.execute(request);
		  System.out.println("Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
	      }
		  sharedSteps.responseContent= result.toString();
		  System.out.println("Response : " + sharedSteps.responseContent);

		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/LoggedUser"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	
	
}