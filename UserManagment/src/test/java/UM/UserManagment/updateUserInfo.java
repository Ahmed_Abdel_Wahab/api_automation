package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.When;
import org.apache.http.client.methods.HttpPost;


public class updateUserInfo{

	String User;
	StringBuffer UserResult = new StringBuffer();

	
	  @When("^I call update user info api$")
	  public void update_user_info() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  //Confirm user API
		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/UpdateUserInfo/UserID");	
		  builder.addParameter("userId", sharedSteps.CreatedUser12Digit.toString().replaceAll("^\"|\"$", ""));
		  builder.addParameter("FirstName", "Automation_update");
		  builder.addParameter("LastName", "Last_update");
		  builder.addParameter("BirthDate", "1993-12-15");
		  builder.addParameter("CountryIso", "us");

		  HttpPost request = new HttpPost (builder.build());
		  
		  // add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  System.out.println("Request : " + request);

		  sharedSteps.response = client.execute(request);
		  System.out.println("Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
	      }
		  sharedSteps.responseContent= result.toString();
		  System.out.println("Response : " + sharedSteps.responseContent);

		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/updateUserInfoResponse"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	
	
}