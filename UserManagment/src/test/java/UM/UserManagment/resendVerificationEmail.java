package UM.UserManagment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import UM.sharedSteps.sharedSteps;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import gherkin.deps.com.google.gson.JsonParser;

import org.apache.http.client.methods.HttpPost;



public class resendVerificationEmail{

	String User;
	StringBuffer UserResult = new StringBuffer();

	
	  @When("^I call resend Verification Email api$")
	  public void resend_Verification_Email() throws Throwable {
		  sharedSteps.get_url();
		  HttpClient client = HttpClientBuilder.create().build();
		  //Confirm user API
		  URIBuilder builder = new URIBuilder(sharedSteps.url+"User/ResendVerificationEmail");	
		  builder.addParameter("email", sharedSteps.UserName.toString().replaceAll("^\"|\"$", "")+"@nagwa.com" );
		  HttpPost request = new HttpPost (builder.build());
		  
		  // add request header
		  request.addHeader("Authorization", sharedSteps.token);
		  request.addHeader("Content-Type", "application/json"); 
		  System.out.println("Request : " + request);

		  sharedSteps.response = client.execute(request);
		  System.out.println("Response Code : " + sharedSteps.response.getStatusLine().getStatusCode());
		  BufferedReader rd = new BufferedReader(new InputStreamReader(sharedSteps.response.getEntity().getContent()));
		  String line = "";
		  StringBuffer result = new StringBuffer();
		  while ((line = rd.readLine()) != null) 
		  {
			  result.append(line);
	      }
		  sharedSteps.responseContent= result.toString();
		  System.out.println("Response : " + sharedSteps.responseContent);
		  
		  //Extract Reset Token for further APIs
	      JsonParser parser = new JsonParser();
	      Object resultObject = parser.parse(sharedSteps.responseContent);
	      JsonObject obj =(JsonObject)resultObject;
	      System.out.println("Status: " + obj.get("success").getAsString());

		  BufferedWriter writer = new BufferedWriter(new FileWriter("./responses/resendVerificationEmail"));
		  writer.write(sharedSteps.responseContent);
		  writer.close();
	  }
	
	
}